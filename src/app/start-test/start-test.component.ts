import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-start-test",
  templateUrl: "./start-test.component.html",
  styleUrls: ["./start-test.component.css"]
})
export class StartTestComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}
  private jobName;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.jobName = params.get("jobName");
      console.log(this.jobName);
    });
  }
}

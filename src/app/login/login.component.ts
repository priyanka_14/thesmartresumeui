import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService) {}
  invalidLogin: boolean;

  signIn(credentials) {
    this.authService.login(credentials).subscribe(
      result => {
        if (result) {
          let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate(["/upload"]);
        } else {
          this.invalidLogin = true;
        }
      },
      error => {
        console.log(error);
        this.invalidLogin = true;
      }
    );
  }
}

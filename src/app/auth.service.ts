import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import "rxjs/add/operator/map";
import { tokenNotExpired } from "angular2-jwt";
const httpOptions = {
  headers: new HttpHeaders({
    Accept: "application/json",
    "Content-Type": "application/json"
  })
};

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  private loginUrl = "http://65.52.197.85:8000/api-token-auth/";

  login(credentials) {
    return this.http
      .post(this.loginUrl, JSON.stringify(credentials), httpOptions)
      .map((response: Response) => {
        if (response && response["token"]) {
          localStorage.setItem("jwt", response["token"]);
          return true;
        }
        return false;
      });
  }

  logout() {
    localStorage.removeItem("jwt");
  }

  isLoggedIn() {
    return tokenNotExpired();
    //return false;
  }
}

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { UploadResumeComponent } from "./upload-resume/upload-resume.component";
import { FormsModule } from "@angular/forms";
import { ResumeActorsComponent } from "./resume-actors/resume-actors.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { HomeComponent } from "./home/home.component";
import { UploadVerifyService } from "./upload-verify.service";
import { HttpClientModule } from "@angular/common/http";
import { ResumeScoreComponent } from "./resume-score/resume-score.component";
import { AuthService } from "./auth.service";
import { LoginComponent } from "./login/login.component";
import { ViewJobsComponent } from "./view-jobs/view-jobs.component";
import { SignupComponent } from "./signup/signup.component";
import { ViewQuestionsComponent } from "./view-questions/view-questions.component";
import { StartTestComponent } from "./start-test/start-test.component";
import { AuthGuardService } from "./auth-guard.service";

@NgModule({
  declarations: [
    AppComponent,
    UploadResumeComponent,
    ResumeActorsComponent,
    NotFoundComponent,
    HomeComponent,
    ResumeScoreComponent,
    LoginComponent,
    ViewJobsComponent,
    SignupComponent,
    ViewQuestionsComponent,
    StartTestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: "", component: HomeComponent },
      { path: "login", component: LoginComponent },
      { path: "signup", component: SignupComponent },
      { path: "upload", component: UploadResumeComponent },
      { path: "actors", component: ResumeActorsComponent },
      { path: "score", component: ResumeScoreComponent },
      { path: "viewjobs", component: ViewJobsComponent },
      { path: "viewquestions/:jobName", component: ViewQuestionsComponent },
      { path: "starttest/:jobName", component: StartTestComponent },
      { path: "**", component: NotFoundComponent }
    ])
  ],
  providers: [UploadVerifyService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}

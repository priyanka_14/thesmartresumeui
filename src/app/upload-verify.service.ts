import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class UploadVerifyService {
  private userUrl = "http://65.52.197.85:8000/gio/";
  private verifyUrl = "http://65.52.197.85:8081/cert_issuer/api/v1.0/verify";

  constructor(private httpClient: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      Accept: "application/json"
    })
  };

  uploadResume(fileToUpload: File) {
    console.log("service call", fileToUpload);
    const formData: FormData = new FormData();
    formData.append("file", fileToUpload, fileToUpload.name);
    const token: any = localStorage.getItem("jwt");
    if (token) {
      this.httpOptions = {
        headers: new HttpHeaders({
          Accept: "application/json",
          Authorization: "JWT " + token
        })
      };
    }

    return this.httpClient.post(
      this.userUrl + "upload_file",
      formData,
      this.httpOptions
    );
  }

  getUserDetails() {
    return this.httpClient.get(this.userUrl + "user_details", this.httpOptions);
  }

  verifyCredentials() {
    return this.httpClient.post(this.verifyUrl, this.httpOptions);
  }

  getSuitableJobs(skills) {
    return this.httpClient.put(
      this.userUrl + "update_user_details",
      skills,
      this.httpOptions
    );
  }

  getAllJobQuestions() {
    return this.httpClient.get("assets/jobQuestions.json", this.httpOptions);
  }
}

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UploadVerifyService } from "../upload-verify.service";

@Component({
  selector: "app-view-jobs",
  templateUrl: "./view-jobs.component.html",
  styleUrls: ["./view-jobs.component.css"]
})
export class ViewJobsComponent implements OnInit {
  constructor(private router: Router, private service: UploadVerifyService) {}
  public relevantJobs;
  ngOnInit() {
    this.service.getUserDetails().subscribe(
      (data: Response) => {
        console.log(data);
        this.relevantJobs = data["details"].relevantJobs;
        console.log(this.relevantJobs);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }
}

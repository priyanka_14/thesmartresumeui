import { Component, OnInit } from "@angular/core";
import { UploadVerifyService } from "../upload-verify.service";

import { Router, ActivatedRoute } from "@angular/router";
import { shouldCallLifecycleInitHook } from "@angular/core/src/view";

@Component({
  selector: "app-view-questions",
  templateUrl: "./view-questions.component.html",
  styleUrls: ["./view-questions.component.css"]
})
export class ViewQuestionsComponent implements OnInit {
  constructor(
    private router: Router,
    private service: UploadVerifyService,
    private route: ActivatedRoute
  ) {}
  private jobQuestions;
  private getAllQuestions: any[];
  public job;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.job = params.get("jobName");
      console.log(this.job);
    });

    this.service.getAllJobQuestions().subscribe(
      data => {
        this.getAllQuestions = data["jobQuestions"];
        console.log(this.getAllQuestions);
        this.jobQuestions = this.getAllQuestions.find(
          job => job.name === this.job
        );
        console.log("questions", this.getAllQuestions, this.jobQuestions);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }
}

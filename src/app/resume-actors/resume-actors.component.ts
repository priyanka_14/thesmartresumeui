import { Component, OnInit } from "@angular/core";
import { UploadVerifyService } from "../upload-verify.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-resume-actors",
  templateUrl: "./resume-actors.component.html",
  styleUrls: ["./resume-actors.component.css"]
})
export class ResumeActorsComponent implements OnInit {
  constructor(private router: Router, private service: UploadVerifyService) {}

  private institutes;
  private contactDetails;

  ngOnInit() {
    this.service.getUserDetails().subscribe(
      (data: Response) => {
        console.log(data);
        this.institutes = data["certs"];
        this.contactDetails = data["details"];
        console.log(this.institutes);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }

  verifyInfo() {
    this.service.verifyCredentials().subscribe(
      data => {
        console.log(data);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
    this.router.navigate(["/score"]);
  }
}

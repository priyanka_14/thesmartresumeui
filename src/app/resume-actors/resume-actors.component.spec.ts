import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeActorsComponent } from './resume-actors.component';

describe('ResumeActorsComponent', () => {
  let component: ResumeActorsComponent;
  let fixture: ComponentFixture<ResumeActorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeActorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeActorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

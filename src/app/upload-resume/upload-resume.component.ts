import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UploadVerifyService } from "../upload-verify.service";
import { Key } from "protractor";

@Component({
  selector: "upload-resume",
  templateUrl: "./upload-resume.component.html",
  styleUrls: ["./upload-resume.component.css"]
})
export class UploadResumeComponent implements OnInit {
  constructor(private router: Router, private service: UploadVerifyService) {}

  fileToUpload: File;
  loading: boolean;
  ngOnInit() {}

  onFileChange(event) {
    this.fileToUpload = event.target.files[0];
  }

  onSubmit() {
    this.loading = true;
    this.service.uploadResume(this.fileToUpload).subscribe(
      data => {
        this.loading = false;
        //console.log(data);
        setTimeout(() => {
          this.router.navigate(["/actors"]);
        }, 1000);
      },
      error => {
        this.loading = false;
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }
}

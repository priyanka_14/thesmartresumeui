import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UploadVerifyService } from "../upload-verify.service";

@Component({
  selector: "app-resume-score",
  templateUrl: "./resume-score.component.html",
  styleUrls: ["./resume-score.component.css"]
})
export class ResumeScoreComponent implements OnInit {
  constructor(private router: Router, private service: UploadVerifyService) {}

  public relevantSkills;
  primarySkills;

  ngOnInit() {
    this.service.getUserDetails().subscribe(
      (data: Response) => {
        console.log(data);
        this.relevantSkills = data["details"].relevantSkills;
        console.log(this.relevantSkills);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }

  viewJobs(details) {
    console.log(details);
    this.service.getSuitableJobs(details).subscribe(
      data => {
        console.log(data);

        this.router.navigate(["/viewjobs"]);
      },
      error => {
        if (error.status == 401) this.router.navigate(["/login"]);
        console.log(error);
      }
    );
  }

}

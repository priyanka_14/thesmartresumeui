import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeScoreComponent } from './resume-score.component';

describe('ResumeScoreComponent', () => {
  let component: ResumeScoreComponent;
  let fixture: ComponentFixture<ResumeScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed, inject } from '@angular/core/testing';

import { UploadVerifyService } from './upload-verify.service';

describe('UploadVerifyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadVerifyService]
    });
  });

  it('should be created', inject([UploadVerifyService], (service: UploadVerifyService) => {
    expect(service).toBeTruthy();
  }));
});
